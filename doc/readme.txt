To run project with dev example data call following commands:
  python manage.py syncdb --noinput
  python manage.py populate_dev_data
  python manage.py runserver
