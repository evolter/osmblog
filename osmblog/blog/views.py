from django.shortcuts import get_object_or_404
from django.views import generic

from osmblog.accounts.models import User
from osmblog.blog import models
from osmblog.utils.views import GetUserMixin


class ArticleView(GetUserMixin, generic.DetailView):
    model = models.Article
    template_name = 'blog/article_detail.html'

    def get_object(self, queryset=None):
        return get_object_or_404(
            self.model,
            created_by__username=self.kwargs.get('username'),
            created_on__year=self.kwargs.get('year'),
            created_on__month=self.kwargs.get('month'),
            created_on__day=self.kwargs.get('day'),
            slug=self.kwargs.get('slug').lower()
        )


class ArticleListView(GetUserMixin, generic.ListView):
    template_name = 'blog/article_list.html'

    def get_queryset(self):
        return models.Article.objects.active(
            created_by__username=self.user.username
        )


class LatestArticleListView(generic.ListView):
    template_name = 'blog/article_list.html'
    context_object_name = 'article_list'
    posts_num = 6

    def get_context_data(self, **kwargs):
        context = super(LatestArticleListView, self).get_context_data(**kwargs)

        page_title = self.kwargs.get('page_title', 'Blog')
        category_slug = self.kwargs.get('category_slug')
        if category_slug:
            page_title = get_object_or_404(models.Category, slug=category_slug)

        context['page_title'] = page_title
        return context

    def get_queryset(self):
        filters = {}

        category_slug = self.kwargs.get('category_slug')
        if category_slug:
            filters['categories__slug'] = category_slug

        if self.kwargs.get('unique_users'):
            user_pks = User.objects.order_by('date_joined').active(
                created_articles__published=True
            ).values_list('pk', flat=True).distinct()[:self.posts_num]
            articles = []
            for user_pk in user_pks:
                articles.append(
                    models.Article.objects.active(created_by=user_pk)
                    .order_by('created_on')[:1][0]
                )
            return articles

        articles = models.Article.objects.active(**filters)
        return articles.order_by('created_on')[:self.posts_num]
