from django.conf.urls import include
from django.conf.urls import patterns
from django.conf.urls import url

from osmblog.blog import views


user_urls = patterns(
    '',

    url(
        r'^$',
        views.ArticleListView.as_view(), name='posts'
    ),
    url(
        (
            r'^(?P<year>[0-9]{4})/'
            r'(?P<month>[0-9]{1,2})/'
            r'(?P<day>[0-9]{1,2})/'
            r'(?P<slug>[-\w]+)/$'
        ),
        views.ArticleView.as_view(), name='post'
    ),
)

urlpatterns = patterns(
    '',

    url(
        r'^by-category/(?P<category_slug>[-\w]+)/$',
        views.LatestArticleListView.as_view(),
        name='by-category'
    ),
    url(
        r'^latest-posts/$',
        views.LatestArticleListView.as_view(),
        kwargs={'page_title': 'Latest Blogs'},
        name='latest-posts'
    ),
    url(
        r'^new-bloggers/$',
        views.LatestArticleListView.as_view(),
        kwargs={'page_title': 'New bloggers', 'unique_users': True},
        name='new-bloggers'
    ),

    url(r'^user/(?P<username>[-\w]+)/', include(user_urls, 'user')),
)
