import random
import factory

from osmblog.accounts.factories import UserFactory
from osmblog.blog import models
from osmblog.utils import factories as utils_factories


class CategoryFactory(factory.DjangoModelFactory):
    FACTORY_FOR = models.Category

    name = factory.Sequence(lambda n: 'Category %d' % n)


class ArticleFactory(factory.DjangoModelFactory):
    FACTORY_FOR = models.Article

    # teaser_image = models.ImageField()
    created_by = factory.SubFactory(UserFactory)
    modified_by = factory.SubFactory(UserFactory)

    @factory.lazy_attribute
    def title(self):
        return utils_factories.random_title()

    @factory.lazy_attribute
    def content(self):
        return utils_factories.random_text(True)


    @factory.post_generation
    def categories(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        categories = []

        if extracted:
            # A list of categories that were passed in.
            categories += extracted

        draw_max = kwargs.get('draw_max')
        if draw_max:
            # Get random ``draw_max`` categories from db.
            category_queryset = models.Category.objects.order_by('?')
            categories += category_queryset[:random.randrange(draw_max) + 1]

        if categories:
            for category in categories:
                self.categories.add(category)
            return

        self.categories.add(CategoryFactory.create(**kwargs))
