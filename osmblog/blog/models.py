from adminsortable.models import Sortable
from ckeditor.fields import RichTextField
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import query
from django.utils.html import strip_tags

from osmblog.accounts.models import User
from osmblog.utils import helpers
from osmblog.utils import models as utils_models


class ArticleManagerMixin(object):
    def active(self, **kwargs):
        return self.filter(published=True).filter(**kwargs)


class ArticleQuerySet(query.QuerySet, ArticleManagerMixin):
    pass


class ArticleManager(models.Manager, ArticleManagerMixin):
    def get_queryset(self):
        return ArticleQuerySet(self.model, using=self._db)


class Article(utils_models.ModelBase):
    title = models.CharField(max_length=80)
    slug = models.SlugField(
        max_length=84, blank=True,
        help_text='Use title by default if not specified.'
    )
    content = RichTextField()
    categories = models.ManyToManyField('Category')
    teaser_content = RichTextField(max_length=1000, blank=True)
    modified_by = models.ForeignKey(
        User, related_name='modified_articles', blank=True
    )
    created_by = models.ForeignKey(
        User, related_name='created_articles', blank=True
    )
    published = models.NullBooleanField()

    objects = ArticleManager()

    def __unicode__(self):
        return self.title

    @property
    def full_slug(self):
        return '/'.join([
            self.created_by.username,
            self.created_on.strftime('%Y/%m/%d'),
            self.slug
        ])

    def get_unique_slug(self):
        queryset = Article.objects.active(
            created_on__year=self.created_on.year,
            created_on__month=self.created_on.month,
            created_on__day=self.created_on.day,
            created_by=self.created_by
        )
        if self.pk:
            queryset = queryset.exclude(pk=self.pk)

        slug = helpers.slugify(self.slug or self.title)
        genuine_slug = slug
        slug_count = 1
        slug_max_len = self._meta.get_field('slug').max_length

        # Find a unique slug (append suffix if needed).
        while not slug or queryset.filter(slug=slug).exists():
            slug_count += 1
            slug_suffix = '-%s' % slug_count
            slug = '%s%s' % (genuine_slug, slug_suffix)
            if len(slug) > slug_max_len:
                slug = slug[:slug_max_len - len(slug_suffix)]
                slug = '%s%s' % (slug, slug_suffix)

        return slug

    @property
    def get_url(self):
        article_url = reverse('blog:user:post', args=self.full_slug.split('/'))
        return article_url + '#disqus_thread'

    def save(self, **kwargs):
        self.populate_dates()
        self.slug = self.get_unique_slug()
        if not self.modified_by:
            self.modified_by = self.created_by
        super(Article, self).save(**kwargs)

    @property
    def short_description(self):
        return helpers.get_shortened_string(
            strip_tags(self.teaser_content or self.content)
        )

    @property
    def teaser(self):
        return self.teaser_content or helpers.get_shortened_string(
            strip_tags(self.content), max_length=400
        )


class Category(utils_models.ModelBase, Sortable):
    name = models.CharField(max_length=40)
    slug = models.SlugField(
        max_length=84, blank=True,
        help_text='Use title by default if not specified.'
    )

    class Meta:
        verbose_name_plural = 'categories'

    def __unicode__(self):
        return self.name

    def get_unique_slug(self):
        queryset = Article.objects.active(
            created_on__year=self.created_on.year,
            created_on__month=self.created_on.month,
            created_on__day=self.created_on.day,
            created_by=self.created_by
        )
        if self.pk:
            queryset = queryset.exclude(pk=self.pk)

        slug = helpers.slugify(self.slug or self.title)
        genuine_slug = slug
        slug_count = 1
        slug_max_len = self._meta.get_field('slug').max_length

        # Find a unique slug (append suffix if needed).
        while not slug or queryset.filter(slug=slug).exists():
            slug_count += 1
            slug_suffix = '-%s' % slug_count
            slug = '%s%s' % (genuine_slug, slug_suffix)
            if len(slug) > slug_max_len:
                slug = slug[:slug_max_len - len(slug_suffix)]
                slug = '%s%s' % (slug, slug_suffix)

        return slug

    @property
    def get_url(self):
        return reverse('blog:by-category', args=(self.slug,))

    def save(self, **kwargs):
        self.slug = helpers.slugify(self.slug or self.name)
        super(Category, self).save(**kwargs)
