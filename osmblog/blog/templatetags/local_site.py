from django import template

from osmblog.blog.models import Category

register = template.Library()


@register.inclusion_tag('blog/includes/categories.html')
def display_categories():
    return {'categories': Category.objects.order_by('order')}
