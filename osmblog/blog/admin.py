from adminsortable.admin import SortableAdmin
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin

from osmblog.blog import models
from osmblog.utils import admin as utils_admin


class ArticleAdmin(utils_admin.DateAdminMixin, admin.ModelAdmin):
    list_display = ['title', 'slug', 'short_description', 'created_by']
    readonly_fields = ['created_by', 'modified_by']

    def get_form(self, request, obj=None, **kwargs):
        form = super(ArticleAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['content'].widget = CKEditorWidget(
            config_name='admin' if request.user.is_superuser else 'mod'
        )
        form.base_fields['teaser_content'].widget = CKEditorWidget(
            config_name='admin' if request.user.is_superuser else 'mod'
        )
        return form

    def save_model(self, request, obj, form, change):
        user = request.user
        if not obj.created_by_id:
            obj.created_by = user
        obj.modified_by = user
        super(ArticleAdmin, self).save_model(request, obj, form, change)


admin.site.register(models.Article, ArticleAdmin)


class CategoryAdmin(utils_admin.DateAdminMixin, SortableAdmin):
    list_display = ['name']


admin.site.register(models.Category, CategoryAdmin)
