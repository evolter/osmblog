class DateAdminMixin(object):
    def get_list_display(self, request):
        list_display = super(DateAdminMixin, self).get_list_display(request)
        return list(list_display or []) + ['created_on', 'modified_on']

    def get_readonly_fields(self, request, obj=None):
        readonly = (
            super(DateAdminMixin, self).get_readonly_fields(request, obj)
        )
        return list(readonly or []) + ['created_on', 'modified_on']
