import random

from django.core.management.base import NoArgsCommand

from osmblog.accounts.factories import UserFactory
from osmblog.accounts.models import User
from osmblog.blog import factories as blog_factories


class Command(NoArgsCommand):
    help = 'Creates default superuser account and test data.'

    def handle(self, **options):
        User.objects.create_superuser('admin', 'admin@example.com2', 'pass')

        user = UserFactory.create(username='mod', is_staff=True)
        blog_factories.ArticleFactory.create(created_by=user, published=True)

        blog_factories.CategoryFactory.create_batch(6)
        for user in UserFactory.create_batch(8):
            blog_factories.ArticleFactory.create_batch(
                random.randint(4, 8),
                created_by=user,
                published=True,
                categories__draw_max=6
            )
