# coding=utf-8
import re

from django.template.defaultfilters import slugify as django_slugify


def get_shortened_string(string, max_length=80, suffix=u'…'):
    shortened_string = string[:max_length + 1]
    if len(shortened_string) > max_length:
        return '%s%s' % (
            shortened_string.rsplit(' ', 1)[0][:max_length], suffix
        )

    return shortened_string


def slugify(slug):
    slug = re.sub(r'^\-+|\-+$', '', django_slugify(unicode(slug)))
    return re.sub('\-{2,}', '-', slug)
