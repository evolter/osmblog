import random

LOREM = (
    (
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin'
        ' blandit consequat elit, ac volutpat nulla molestie quis. Morbi'
        ' blandit nulla at turpis fermentum ultricies. Sed hendrerit ipsum'
        ' elit, non dictum ligula tincidunt sed. Sed ac nisl sed sapien'
        ' molestie venenatis. Fusce auctor sollicitudin ipsum, vel fringilla'
        ' turpis pulvinar et. Vestibulum commodo, urna vel elementum ultrices,'
        ' mauris enim facilisis ipsum, in varius turpis nisl a diam. Sed'
        ' rhoncus faucibus sodales. Cras pulvinar mollis sapien. Nunc porta'
        ' libero feugiat, eleifend tellus quis, laoreet nisl. Proin'
        ' scelerisque erat eget purus feugiat, malesuada gravida felis'
        ' vehicula. Duis lectus urna, pretium vitae ligula eu, mattis'
        ' sollicitudin urna. Vivamus facilisis sit amet dolor eget lacinia.'
        ' Mauris tempus mi massa, sed eleifend felis ornare eu. Duis semper'
        ' magna in mi eleifend dapibus. Pellentesque accumsan quis enim in'
        ' cursus'
    ), (
        'Duis commodo semper nulla, dapibus lobortis magna rutrum nec. Aenean'
        ' dapibus viverra nibh. Suspendisse quis enim vehicula, dapibus orci'
        ' quis, fringilla tellus. In malesuada elit non erat rutrum congue.'
        ' Nulla eget euismod eros. Nunc porta libero ipsum, et vulputate metus'
        ' rutrum commodo. Mauris vel pharetra mi. Maecenas nec leo risus.'
        ' Proin auctor scelerisque est vel elementum. Donec id massa lacus.'
        ' Nullam varius ipsum et dui varius dignissim. Aenean vitae quam leo.'
        ' Maecenas vitae mollis velit, id gravida massa. Quisque tincidunt'
        ' rutrum purus ut commodo'
    ), (
        'Vestibulum dignissim elementum ipsum. Aliquam massa magna, dictum'
        ' condimentum accumsan id, posuere sit amet dolor. Pellentesque sit'
        ' amet facilisis nibh. Vestibulum nec augue luctus, sollicitudin'
        ' turpis in, rhoncus ligula. Nulla congue scelerisque tellus a'
        ' pellentesque. Vestibulum sed est sit amet dolor congue sagittis ac'
        ' id est. Maecenas dignissim consequat enim id consequat. Cras libero'
        ' massa, mollis non neque nec, porta sagittis felis. Aenean at diam'
        ' ornare, posuere mi id, rutrum est. Mauris ac gravida felis. Ut a'
        ' tempor mauris, non lobortis tortor. Nullam faucibus orci a metus'
        ' porttitor eleifend'
    ), (
        'Phasellus volutpat iaculis felis sed tristique. Donec eget mollis'
        ' augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut'
        ' varius dui lorem, at ultrices dui gravida in. Maecenas auctor id'
        ' urna et imperdiet. Praesent sit amet faucibus urna. Nullam suscipit'
        ' nibh sem, eu fermentum ligula suscipit in. Praesent rhoncus, nunc ac'
        ' aliquam imperdiet, arcu mauris pulvinar ligula, ut ultrices massa'
        ' metus eget justo. Aliquam sagittis metus sed lectus ornare tempus.'
        ' Mauris eget ullamcorper purus, sit amet auctor ligula. Morbi posuere'
        ' ultrices arcu id mollis. Suspendisse potenti. Quisque magna sem,'
        ' lobortis a enim sed, dictum dignissim erat. Quisque et velit'
        ' vestibulum, eleifend mi ut, pharetra tortor. Aenean tincidunt'
        ' luctus augue sit amet facilisis. Curabitur non nulla vel sem'
        ' vestibulum tincidunt sed ut est'
    ), (
        'Cras et magna volutpat, porttitor est lacinia, condimentum augue.'
        ' Aenean accumsan imperdiet odio venenatis congue. Morbi cursus leo'
        ' magna, a rhoncus tellus tincidunt in. Nulla orci arcu, gravida eget'
        ' bibendum vel, bibendum laoreet dui. Etiam vestibulum tempus enim.'
        ' Vivamus sagittis erat ac mi mollis vehicula. Duis ipsum urna, dictum'
        ' quis ornare id, pretium pellentesque eros'
    )
)


def random_text(html=False):
    paragraphs_count = len(LOREM)
    paragraphs_number = random.randrange(paragraphs_count) + 1
    texts = []
    while paragraphs_number > 0:
        paragraphs_number -= 1
        paragraph = LOREM[random.randrange(paragraphs_count)]
        sentences_count = paragraph.count('.') + 1
        texts.append(
            '%s.' % paragraph.rsplit('.', random.randrange(sentences_count))[0]
        )

    template = '%s'
    separator = '\n\n'
    if html:
        template = '<p>%s</p>'
        separator = '</p><p>'

    return template % separator.join(texts)


def random_title():
    titles = LOREM[0].split('. ')
    return titles[random.randrange(len(titles))]
