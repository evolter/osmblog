from django.shortcuts import get_object_or_404

from osmblog.accounts.models import User


class GetUserMixin(object):
    user = None

    def dispatch(self, request, *args, **kwargs):
        self.user = get_object_or_404(User, username=kwargs.get('username'))
        return super(GetUserMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(GetUserMixin, self).get_context_data(**kwargs)
        context['site_user'] = self.user
        return context
