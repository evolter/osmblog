from datetime import datetime

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.timezone import utc


class ModelBase(models.Model):
    """Custom model base containing common fields."""
    _dates_populated = False

    class Meta:
        abstract = True

    created_on = models.DateTimeField(
        db_index=True,
        null=True,
        blank=True
    )
    modified_on = models.DateTimeField(
        db_index=True,
        null=True,
        blank=True
    )
    removed_on = models.DateTimeField(
        db_index=True,
        null=True,
        blank=True
    )
    removed_by = models.ForeignKey(
        get_user_model(),
        null=True, blank=True,
        on_delete=models.SET_NULL
    )

    def populate_dates(self):
        now = datetime.utcnow().replace(tzinfo=utc)
        if not self.created_on:
            self.created_on = now
        self.modified_on = now
        self._dates_populated = True

    def save(self, **kwargs):
        if not self._dates_populated:
            self.populate_dates()
        super(ModelBase, self).save(**kwargs)
