# coding=utf-8
from django.test import TestCase
from osmblog.utils.helpers import get_shortened_string


class GetShortenedStringTest(TestCase):
    def setUp(self):
        self.test_string = 'This is some meaningless text for tests.'

    def test_unchanged_string(self):
        """Test that text is not changed if did not exceed max length."""
        shortened_string = get_shortened_string(self.test_string, 80)
        self.assertEqual(shortened_string, self.test_string)

    def test_shortened_string(self):
        """Test that long text was cut in expected place."""
        shortened_string = get_shortened_string(self.test_string, 7)
        self.assertEqual(shortened_string, u'This is…')

    def test_string_wo_white_spaces(self):
        """Test that long words w/o spaces do not exceed max length."""
        self.test_string = 'ThisIsSomeLongTextWithoutWhiteSpaces.'
        shortened_string = get_shortened_string(self.test_string, 17)
        self.assertEqual(shortened_string, u'ThisIsSomeLongTex…')

    def test_suffix(self):
        """Test that expected suffix was returned."""
        shortened_string = get_shortened_string(self.test_string, 7, '[..]')
        self.assertEqual(shortened_string, 'This is[..]')
