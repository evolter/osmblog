from django.views.generic import RedirectView


class HomeView(RedirectView):
    pattern_name = 'blog:latest-posts'

    def get_redirect_url(self, *args, **kwargs):
        return super(HomeView, self).get_redirect_url(*args, **kwargs)
