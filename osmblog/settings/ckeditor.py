CKEDITOR_UPLOAD_PATH = 'uploads/'

CKEDITOR_TOOLBAR_CONFIGS = {
    'Full': [
        {'name': 'document', 'items': [
            'Source', '-', 'Save', 'NewPage', 'DocProps', 'Preview',
            'Print', '-', 'Templates'
        ]},
        {'name': 'clipboard', 'items': [
            'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-',
            'Undo', 'Redo'
        ]},
        {'name': 'editing', 'items': [
            'Find', 'Replace', '-', 'SpellChecker', 'Scayt'
        ]},
        {'name': 'forms', 'items': [
            'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select',
            'Button', 'ImageButton', 'HiddenField'
        ]},
        '/',
        {'name': 'styles', 'items':
            ['Styles', 'Format', 'Font', 'FontSize']},
        {'name': 'insert', 'items':
            ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley',
             'SpecialChar', 'PageBreak']},
        {'name': 'tools', 'items':
            ['Maximize', 'ShowBlocks', '-', 'About']},
        {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
        '/',
        {'name': 'basicstyles', 'items':
            ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript',
             'Superscript']},
        {'name': 'paragraph', 'items':
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
             '-', 'NumberedList', 'BulletedList',
             '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-',
             'BidiLtr', 'BidiRtl']},
        {'name': 'colors', 'items':
            ['TextColor', 'BGColor']},
        ['SelectAll', '-', 'RemoveFormat'],
    ],
    'Advanced': [
        {'name': 'document', 'items':
            ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-',
             'Templates']},
        {'name': 'clipboard', 'items':
            ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo',
             'Redo']},
        {'name': 'editing', 'items':
            ['Find', 'Replace', '-', 'SpellChecker', 'Scayt']},
        {'name': 'tools', 'items':
            ['Maximize', 'ShowBlocks', '-', 'About']},
        '/',
        {'name': 'styles', 'items':
            ['Styles', 'Format', 'Font', 'FontSize']},
        {'name': 'insert', 'items':
            ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar',
             'PageBreak']},
        {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
        '/',
        {'name': 'basicstyles', 'items':
            ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript',
             'Superscript']},
        {'name': 'paragraph', 'items':
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
             '-', 'NumberedList',
             'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote',
             'CreateDiv']},
        {'name': 'colors', 'items':
            ['TextColor', 'BGColor']},
        ['SelectAll', '-', 'RemoveFormat']
    ],
    'Standard': [
        {'name': 'document', 'items':
            ['Source', '-', 'Templates']},
        {'name': 'clipboard', 'items':
            ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo',
             'Redo']},
        {'name': 'editing', 'items':
            ['Find', 'Replace', '-', 'SpellChecker', 'Scayt']},
        {'name': 'tools', 'items':
            ['Maximize', 'ShowBlocks', '-', 'About']},
        '/',
        {'name': 'styles', 'items':
            ['Styles', 'Format', 'FontSize']},
        {'name': 'insert', 'items':
            ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar']},
        ['SelectAll', '-', 'RemoveFormat'],
        {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
        '/',
        {'name': 'basicstyles', 'items':
            ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript',
             'Superscript']},
        {'name': 'paragraph', 'items':
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
             '-', 'NumberedList',
             'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
        {'name': 'colors', 'items':
            ['TextColor', 'BGColor']},
    ],
    'Simple': [
        {'name': 'clipboard', 'items':
            ['Cut', 'Copy', 'Paste', '-', 'Undo', 'Redo']},
        {'name': 'links', 'items':
            ['Link', 'Unlink', 'Anchor']},
        {'name': 'insert', 'items':
            ['Image', 'HorizontalRule', 'Smiley', 'SpecialChar']},
        {'name': 'tools', 'items':
            ['Maximize', 'ShowBlocks', '-', 'About']},
        '/',
        {'name': 'paragraph', 'items':
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
             '-', 'NumberedList',
             'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
        {'name': 'colors', 'items':
            ['TextColor', 'BGColor']},
        ['SelectAll', '-', 'RemoveFormat'],
        '/',
        {'name': 'basicstyles', 'items':
            ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript',
             'Superscript']},
        {'name': 'styles', 'items':
            ['Styles', 'FontSize']},
    ],
    'Basic': [
        {'name': 'basicstyles', 'items':
            ['Bold', 'Italic', 'Underline', 'Strike']},
        {'name': 'paragraph', 'items':
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
    ]
}

CKEDITOR_CONFIGS = {
    'full': {
        'toolbar': CKEDITOR_TOOLBAR_CONFIGS['Full'],
        'height': 240,
        'width': 820,
        'filebrowserWindowWidth': 940,
        'filebrowserWindowHeight': 725,
    },
    'admin': {
        'toolbar': CKEDITOR_TOOLBAR_CONFIGS['Advanced'],
        'height': 200,
        'width': 660,
        'filebrowserWindowWidth': 940,
        'filebrowserWindowHeight': 725,
    },
    'mod': {
        'toolbar': CKEDITOR_TOOLBAR_CONFIGS['Standard'],
        'height': 160,
        'width': 600,
        'filebrowserWindowWidth': 940,
        'filebrowserWindowHeight': 725,
    },
    'user': {
        'toolbar': CKEDITOR_TOOLBAR_CONFIGS['Simple'],
        'height': 120,
        'width': 480,
        'filebrowserWindowWidth': 940,
        'filebrowserWindowHeight': 725,
    },
    'default': {
        'toolbar': CKEDITOR_TOOLBAR_CONFIGS['Basic'],
        'height': 120,
        'width': 400,
    }
}
