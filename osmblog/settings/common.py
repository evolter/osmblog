PROJECT_NAME = 'osmblog'

import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
root_path = lambda *path: os.path.normpath(os.path.join(BASE_DIR, *path))
