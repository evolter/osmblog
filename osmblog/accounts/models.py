from django.contrib.auth.models import AbstractUser
from django.db import IntegrityError
from django.db import models
from django.db.models import query

GENDER_CHOICES = (
    ('u', 'unknown'),
    ('f', 'female'),
    ('m', 'male'),
)


class UserManagerMixin(object):
    def active(self, **kwargs):
        return self.filter(is_active=True).filter(**kwargs)


class UserQuerySet(query.QuerySet, UserManagerMixin):
    pass


class UserManager(models.Manager, UserManagerMixin):
    def get_queryset(self):
        return UserQuerySet(self.model, using=self._db)


class User(AbstractUser):
    dob = models.DateField(null=True)
    gender = models.CharField(
        max_length=1, choices=GENDER_CHOICES, default=GENDER_CHOICES[0][0]
    )

    objects = UserManager()

    def validate_unique(self, exclude=None):
        super(User, self).validate_unique(exclude)

    def save(self, *args, **kwargs):
        if self.email and (
            User.objects.exclude(pk=self.pk).filter(email=self.email).exists()
        ):
            raise IntegrityError('column email is not unique')

        super(User, self).save(*args, **kwargs)
