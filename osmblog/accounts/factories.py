import datetime
import factory
from osmblog.accounts import models as accounts_models

GENDER_KEYS = [k for k, v in accounts_models.GENDER_CHOICES]


class UserBaseFactory(factory.DjangoModelFactory):
    FACTORY_FOR = accounts_models.User


class UserFactory(UserBaseFactory):
    username = factory.Sequence(lambda n: 'user%d' % n)
    first_name = factory.Sequence(lambda n: 'Name%d' % n)
    last_name = factory.Sequence(lambda n: 'Surname%d' % n)
    gender = factory.Iterator(
        accounts_models.GENDER_CHOICES, getter=lambda c: c[0]
    )

    @classmethod
    def _prepare(cls, create, **kwargs):
        user = super(UserFactory, cls)._prepare(create, **kwargs)
        password = kwargs.pop('password', 'pass')
        if password:
            user.set_password(password)
        if create:
            user.save()
        return user

    @factory.sequence
    def dob(self):
        year = datetime.date.today().year
        return datetime.date(year-20, 2, 3)

    @factory.lazy_attribute
    def email(self):
        first_name = self.first_name.lower()
        last_name = self.last_name.lower()
        return '%s.%s@example.com' % (first_name, last_name)
